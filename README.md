
## របារកម្មវិធី Zoom Sliver App Bar

[![License](https://img.shields.io/badge/license-MIT-green.svg)](/LICENSE)
[![Platform Flutter](https://img.shields.io/badge/platform-Flutter-blue.svg)](https://flutter.dev)

[![alt](https://icons.iconarchive.com/icons/custom-icon-design/mono-business-2/32/coffee-icon.png)ទិញកាហ្វេអោយខ្ញុំ? Buy me a cup of coffee?](https://www.buymeacoffee.com/sivhong007)


English language below


## ភាសាខ្មែរ 

### របារកម្មវិធីដែលអាចធ្វើបានយ៉ាងងាយស្រួល។ គ្រាន់​តែ​នាំ​ចូល​ហើយយកប្រើ​ជាការ​ស្រេច​។ ចំណងជើងមានចលនាយ៉ាងរលូន និងការផ្លាស់ប្តូរទំហំពុម្ពអក្សរចំណងជើង។ 

អ្នកអាចប្រើវាបានដោយ៖
```
 dart pub add zoom_sliver_app_bar
```

ឬ

```
 flutter pub add zoom_sliver_app_bar
```





## កូដគំរូ


```dart
   NestedScrollView(
          headerSliverBuilder: (context, innerBoxIsScrolled) {
            return [
              const ZoomSliverAppBarWidget(
                title: "....",
              ),
            ];
          },
   );
```
---


Khmer language above
## English 

### An effortlessly implementable silver app bar. Simply import and it's ready to go. Its smoothly scaling animation and dynamic title font resizing enhance user-friendliness and convenience.



You can use it by:
```
 dart pub add zoom_sliver_app_bar
```

Or

```
 flutter pub add zoom_sliver_app_bar
```

## Sample Code


```dart
  NestedScrollView(
          headerSliverBuilder: (context, innerBoxIsScrolled) {
            return [
              const ZoomSliverAppBarWidget(
                title: "....",
              ),
            ];
          },
   );
```

![alt text](https://gitlab.com/SIVHONG1/zoom_sliver_app_bat/-/raw/main/screenshot.gif)


MIT License

Copyright (c) 2024 SIVHONG DEAB

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

#### pagination scroll list vertical refresh loading pulling pull to refresh loadmore infinite scrolling load more & lazy loading