import 'dart:io';

import 'package:flutter/material.dart';

class ZoomSliverAppBarWidget extends StatelessWidget {
  const ZoomSliverAppBarWidget({
    Key? key,
    required this.title,
    this.leftIcon,
    this.rightIcon,
    this.paddingTitleBottom = 10,
    this.maxTitleSize = 24,
    this.minTitleSize = 18,
    this.backgroundColor = Colors.white,
    this.titleColor = const Color(0xFF101828),
    this.titleFontWeight = FontWeight.w600,
    this.leftTitle = true,
    this.maxExtent = 100,
    this.paddingLeft = 16,
    this.top = 0,
    this.right = 0,
    this.left = 0,
    this.isLoading = false,
    this.loadingWidget,
  }) : super(key: key);
  final String title;
  final List<Widget>? leftIcon;
  final List<Widget>? rightIcon;
  final double paddingTitleBottom;
  final double maxTitleSize;
  final double minTitleSize;
  final Color backgroundColor;
  final Color titleColor;
  final FontWeight titleFontWeight;
  final bool leftTitle;
  final double maxExtent;
  final double paddingLeft;
  final double top;
  final double right;
  final double left;
  final bool isLoading;
  final Widget? loadingWidget;

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      pinned: true,
      delegate: SliverHeader(
        title: title,
        leftIcon: leftIcon,
        rightIcon: rightIcon,
        paddingTitleBottom: paddingTitleBottom,
        maxTitleSize: maxTitleSize,
        minTitleSize: minTitleSize,
        backgroundColor: backgroundColor,
        titleColor: titleColor,
        titleFontWeight: titleFontWeight,
        leftTitle: leftTitle,
        maxExtentHeight: maxExtent,
        paddingLeft: paddingLeft,
        top: top,
        right: right,
        left: left,
        isLoading: isLoading,
        loadingWidget: loadingWidget,
      ),
    );
  }
}

class SliverHeader extends SliverPersistentHeaderDelegate {
  final double maxExtentHeight;
  final String title;

  final List<Widget>? leftIcon;
  final List<Widget>? rightIcon;
  final double paddingTitleBottom;
  final double maxTitleSize;
  final double minTitleSize;
  final Color backgroundColor;
  final Color titleColor;
  final FontWeight titleFontWeight;
  final bool leftTitle;
  final double paddingLeft;
  final double top;
  final double right;
  final double left;
  final bool isLoading;
  final Widget? loadingWidget;

  SliverHeader({
    required this.title,
    this.leftIcon,
    this.rightIcon,
    required this.paddingTitleBottom,
    required this.maxTitleSize,
    required this.minTitleSize,
    required this.backgroundColor,
    required this.titleColor,
    required this.titleFontWeight,
    required this.leftTitle,
    required this.maxExtentHeight,
    required this.paddingLeft,
    required this.top,
    required this.right,
    required this.left,
    required this.isLoading,
    this.loadingWidget,
  });

  double scrollingValue = 0;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    scrollingValue = (shrinkOffset > maxExtentHeight ? maxExtentHeight : shrinkOffset) / maxExtentHeight;

    return Container(
      color: backgroundColor,
      child: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(
              left: paddingLeft,
              bottom: paddingTitleBottom - (paddingTitleBottom * scrollingValue),
              right: paddingLeft,
            ),
            child: Row(
              children: [
                SizedBox(
                  width: maxTitleSize - ((maxTitleSize - minTitleSize) * scrollingValue) == minTitleSize
                      ? 24 + paddingLeft
                      : 0,
                ),
                Expanded(
                  child: Align(
                    alignment:
                        // left title: move x and y axis
                        leftTitle
                            ? Alignment(
                                -1 + scrollingValue,
                                (-1 + scrollingValue) / -1,
                              )
                            :
                            // center title: move only y axis
                            Alignment(0, (-1 + scrollingValue) / -1),
                    child: isLoading
                        ? loadingWidget ?? const LinearProgressIndicator()
                        : Text(
                            title,
                            style: Theme.of(context).textTheme.headlineSmall?.copyWith(
                                  color: titleColor,
                                  fontWeight: titleFontWeight,
                                  // scale font base on scrolling value
                                  fontSize: maxTitleSize - ((maxTitleSize - minTitleSize) * scrollingValue),
                                  height: 1,
                                ),
                            maxLines: scrollingValue > 0.2 ? 1 : 2,
                            overflow: TextOverflow.ellipsis,
                          ),
                  ),
                ),
                SizedBox(
                  width: maxTitleSize - ((maxTitleSize - minTitleSize) * scrollingValue) == minTitleSize
                      ? 24 + paddingLeft
                      : 0,
                ),
              ],
            ),
          ),
          Positioned(
            top: top,
            left: left,
            child: Row(
              children: leftIcon ??
                  [
                    InkWell(
                      onTap: () => Navigator.maybePop(context),
                      borderRadius: BorderRadius.circular(50),
                      child: Ink(
                        decoration: BoxDecoration(
                          color: backgroundColor,
                          shape: BoxShape.circle,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(12),
                          child: Icon(
                            Platform.isIOS ? Icons.arrow_back_ios : Icons.arrow_back,
                            color: const Color(0xFF101828),
                          ),
                        ),
                      ),
                    )
                  ],
            ),
          ),
          Positioned(
            top: top,
            right: right,
            child: Row(
              children: rightIcon ?? [const SizedBox()],
            ),
          ),
        ],
      ),
    );
  }

  @override
  double get maxExtent => maxExtentHeight;

  @override
  double get minExtent => kToolbarHeight;

  @override
  bool shouldRebuild(covariant SliverHeader oldDelegate) {
    return title != oldDelegate.title || leftIcon != oldDelegate.leftIcon || rightIcon != oldDelegate.rightIcon;
  }
}
